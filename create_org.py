from sro_db.model.organization.models import *
from sro_db.model.core.models import *
from sro_db.service.organization.service import *
from sro_db.service.core.service import *
from pprint import pprint 

#Criando os dados do prodest
organization = Organization()
organization.name = "EMPRESA_DE_TESTE"
organization.description = "Apenas testando"

organizationService = OrganizationService()
organizationService.create(organization)

applicationService = ApplicationService()
jira = applicationService.retrive_by_name("jira")

#criando a applicação
configuration = Configuration()
configuration.name = "Configuação de teste para o Jira"
configuration.description = "Configuação de teste para o Jira"
configuration.user = "lucasmoraesplay@gmail.com"
configuration.secret = "seApnbFfBXp6AVCdanCK8DFB"
configuration.url =  "https://ledszeppellin.atlassian.net/"
configuration.application = jira
configuration.organization = organization

configurationService = ConfigurationService()
configurationService.create(configuration)

print('data = {')
print(f"    'user': '{configuration.user}',")
print(f"    'key': '{configuration.secret}',")
print(f"    'url': '{configuration.url}',")
print(f"    'organization_id': '{str(organization.uuid)}',")
print(f"    'configuration_id': '{str(configuration.uuid)}',")
print("}")
